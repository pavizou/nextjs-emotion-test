/* eslint-disable no-restricted-syntax */
/* eslint-disable no-param-reassign */
import { HtmlEntities } from 'utils/string';

const createJsonLdElms = (news, generalSettings) => {
  // console.log('news.seo :>> ', news.seo);
  const caption = news.featuredImage && news.featuredImage.node.caption
    ? news.featuredImage.node.caption.match(/<p>(.*)<\/p>/i)
    : '';
  return {
    '@context': 'https://schema.org',
    '@graph': [
      {
        '@type': 'WebSite',
        '@id': `${generalSettings.url}/#website`,
        url: `${generalSettings.url}/`,
        name: '',
        description: 'Toutes vos infos concerts, musiques et festivals en France',
        potentialAction: [
          {
            '@type': 'SearchAction',
            target: `${generalSettings.url}/?query={search_term_string}`,
            'query-input': 'required name=search_term_string',
          },
        ],
        inLanguage: generalSettings.language,
      },
      {
        '@type': 'ImageObject',
        '@id': `${news.link}#primaryimage`,
        inLanguage: generalSettings.language,
        url: news.featuredImage && news.featuredImage.node.sourceUrl,
        width: news.featuredImage && news.featuredImage.node.mediaDetails.width,
        height: news.featuredImage && news.featuredImage.node.mediaDetails.height,
        caption: caption !== null ? caption[1] : '',
      },
      {
        '@type': 'WebPage',
        '@id': `${news.link}#webpage`,
        url: `${news.link}`,
        name: news.seo.title,
        isPartOf: {
          '@id': `${generalSettings.url}/#website`,
        },
        primaryImageOfPage: {
          '@id': `${news.link}#primaryimage`,
        },
        datePublished: news.seo.opengraphPublishedTime,
        dateModified: news.seo.opengraphModifiedTime,
        author: {
          '@id': `${generalSettings.url}/#/schema/person/${news.author.node.id}`,
        },
        description: news.seo.metaDesc,
        inLanguage: generalSettings.language,
        potentialAction: [
          {
            '@type': 'ReadAction',
            target: [news.link],
          },
        ],
      },
      {
        '@type': 'Person',
        '@id': `${generalSettings.url}/#/schema/person/${news.author.node.id}`,
        name: news.author.node.name,
        image: {
          '@type': 'ImageObject',
          '@id': `${generalSettings.url}/#personlogo`,
          inLanguage: generalSettings.language,
          url: news.author.node.avatar.url,
          caption: news.author.node.name,
        },
        description: news.author.node.description,
      },
    ],
    '@type': 'NewsArticle',
    mainEntityOfPage: {
      '@type': 'WebPage',
      '@id': `${news.link}#webpage`,
    },
    headline: news.title.substring(0, 100),
    image: news.featuredImage && news.featuredImage.node.sourceUrl,
    datePublished: `${news.dateGmt}+00:00`,
    dateModified: `${news.modifiedGmt}+00:00`,
    author: {
      '@id': `${generalSettings.url}/#/schema/person/${news.author.node.id}`,
      '@type': 'Person',
      name: news.author.node.name,
    },
    publisher: {
      '@type': 'Organization',
      name: 'Hey Alex',
      logo: {
        '@type': 'ImageObject',
        url: 'https://hey-alex.fr/actu/wp-content/uploads/2018/08/logopixel.jpg',
      },
    },
  };
};

const createMetaTags = (news, generalSettings) => {
  // console.log('news.seo :>> ', news.seo);
  // console.log('news.seo.metaRobotsNoindex :>> ', news.seo.metaRobotsNoindex);
  // console.log('news.seo.metaRobotsNofollow :>> ', news.seo.metaRobotsNofollow);
  const ogImage = news.seo.opengraphImage;
  return [
    {
      name: 'description',
      content: news.seo.metaDesc,
    },
    {
      name: 'robots',
      content: `${news.seo.metaRobotsNoindex}, ${news.seo.metaRobotsNofollow}, max-snippet:-1, max-image-preview:large, max-video-preview:-1`,
    },
    {
      property: 'og:locale',
      content: generalSettings.language,
    },
    {
      property: 'og:type',
      content: news.seo.opengraphType,
    },
    {
      property: 'og:title',
      content: news.seo.opengraphTitle,
    },
    {
      property: 'og:description',
      content: news.seo.opengraphDescription,
    },
    {
      property: 'og:url',
      content: news.seo.opengraphUrl,
    },
    {
      property: 'article:publisher',
      content: news.seo.opengraphPublisher,
    },
    {
      property: 'article:published_time',
      content: news.seo.opengraphPublishedTime,
    },
    {
      property: 'article:modified_time',
      content: news.seo.opengraphModifiedTime,
    },
    {
      property: 'og:image',
      content: ogImage && ogImage.mediaItemUrl,
    },
    {
      property: 'og:image:width',
      content: ogImage && ogImage.mediaDetails.width,
    },
    {
      property: 'og:image:height',
      content: ogImage && ogImage.mediaDetails.height,
    },
    {
      name: 'twitter:card',
      content: 'summary',
    },
    {
      name: 'twitter:creator',
      content: '@Hey_Alex_site',
    },
    {
      name: 'twitter:site',
      content: '@Hey_Alex_site',
    },
  ];
};

const setLeadParagraph = (news) => {
  if (news.content) {
    const m = news.content.match(/<p.*?>(.*?)<\/p>/i);
    news.leadParagraph = m !== null
      ? m[1]
        .replace(/<\/?span.*?>/, '')
        .replace(/<script.*?>.*?<\/script>/gism, '')
      : '';
  }
  return news;
};

const setMetaTags = generalSettings => (news) => {
  news.metaTags = createMetaTags(news, generalSettings);
  return news;
};

const setJsonLd = generalSettings => (news) => {
  news.jsonLd = createJsonLdElms(news, generalSettings);
  return news;
};

const setMetaTitle = (news) => {
  news.metaTitle = news.seo.title;
  return news;
};

const setArtistSlugs = (news) => {
  const artistMatches = news.content.match(/hey-alex\.fr\/artiste\/(.[^?"]*)/gim); // ".*?hey-alex\.fr\/artiste\/(.[^?]*?).*?"
  news.artistSlugs = artistMatches !== null && artistMatches.length > 0 ? [...new Set(artistMatches.map(m => m.split('/')[2]))] : null;
  return news;
};

const decodeFieldHtml = field => (news) => {
  news[field] = HtmlEntities.decode(news[field]);
  return news;
};

const setNeedExternalSnippet = (news) => {
  if (news.content.indexOf('platform.twitter.com/widgets.js') !== -1) {
    news.needsTwitter = true;
  }
  if (news.content.indexOf('connect.facebook.net') !== -1) {
    news.needsFacebook = true;
  }
  if (news.content.indexOf('www.instagram.com/embed.js') !== -1) {
    news.needsInstagram = true;
  }
  return news;
};

const adaptHtml = (news) => {
  news.content = news.content.replace(/<script.*?>.*?<\/script>/gism, '')
    .replace(/(<iframe .*><\/iframe>)/ig, '<div class="fluid-video-wrapper">$1</div>')
    .replace(/href="https:\/\/hey-alex.fr\/actu/ig, 'href="/posts')
    .replace(/<p.*?>(.*?)<\/p>/i, '');
  return news;
};

const addForwardedQueryString = (search, referer) => (news) => {
  if ((search && (search.indexOf('utm_') !== -1 || search.indexOf('gclid') !== -1))) {
    // console.log('utm tags or referrer found in current document');
    let query = '';
    if (search) query = search.substring(1);
    let fwQuery = query.replace(/utm/g, 'orig_utm').replace(/gclid=([^&]*)/g, 'orig_gclid=$1&gclid=$1');
    if (referer !== '' && typeof referer !== 'undefined') {
      if (fwQuery === '') fwQuery += `orig_referrer=${referer}`;
      else fwQuery += `&orig_referrer=${referer}`;
    }

    news.content = news.content.replace(/(https:\/\/(?:next-concert|live-booker).[^"]*)/gim, `$1&${fwQuery}`);
  }
  return news;
};

export default {
  setLeadParagraph,
  setMetaTags,
  setJsonLd,
  setMetaTitle,
  setArtistSlugs,
  decodeFieldHtml,
  setNeedExternalSnippet,
  adaptHtml,
  // setArticleSchema,
  addForwardedQueryString,
};

const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);
export { pipe }; // eslint-disable-line

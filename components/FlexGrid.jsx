// import Image from 'next/image';
import Link from 'next/link';
import { css, keyframes } from '@emotion/react'
import Image from 'next/image';
import HalfCircle from './HalfCircle';

const hoverAnim = keyframes`
from {
  transform: rotate3d(0, 0, 0, 0deg);
}
50% {
  transform: rotate3d(0, 1, 0, 180deg) scale(1.10);
  
}
to {
  transform: rotate3d(0, 0, 0, 0deg) scale(1.20);
}
`;

export default function FlexGrid({ items }) {
  return (
    <div
      className="cc flex mx-1 flex-wrap w-full h-screen items-center justify-center pointer-events-none"
      css={css`
        /* padding-right: -30rem; */
        &:hover > div:not(:hover) {
          opacity: 0.5;
      }
        &:hover > div {
            margin-left: 1.25rem;
            margin-right: 1.25rem;
        }
      `}
    >
        {
          items.map((item, i) => {
            return (
              <div
                className="box-border pointer-events-auto font-mono flex flex-grow flex-col sm:-ml-2 bg-[#17141d] rounded-3xl my-4 h-[400px] max-w-xs transform-gpu transition-all duration-300 hover:z-10 hover:scale-[1.20] shadow-cards-light dark:shadow-cards"
                // className="flex flex-grow text-center transition-all duration-300 transform hover:z-10 lg:hover:mx-3 md:hover:scale-110 shadow-cards-light dark:shadow-cards flex-col rounded bg-gray-50 dark:bg-gray-900 dark:text-gray-200 w-96 h-72 max-w-full mx-3 sm:-ml-5 my-2 md:my-4 py-2 px-2 items-center justify-center"
                key={[item.slug]}
                // css={css`
                //   :hover {
                //     transform: scale(1.20) translate(1px);
                //     z-index: 10;
                //     transition-duration: 300ms;
                //   }
                // `}
              >
                <Link href={`/posts/${item.slug}`}>
                  <a className="flex-shrink px-6 py-6">
                    <div className="font-mono">
                      <span className="font-bold">Article </span><span className="text-gray-400">{item.date_gmt}</span>
                    </div>
                    <div
                      className="max-w-xs -mx-6 my-4 h-40 relative"
                      css={css`
                        background-image: url(${item.uagb_featured_image_src?.['list-block']?.[0]});
                      `}
                    >
                      {/* <Image src={item.uagb_featured_image_src?.['list-block']?.[0]} layout="fill" /> */}
                    </div>
                    {/* <div
                      className="m-2 h-20 w-20 relative mx-auto"
                    >
                      <div
                        className="rounded-full m-auto bg-center bg-contain"
                        aria-label="Article image"
                        style={{
                          height: '62px',
                          width: '62px',
                          backgroundImage: `url(${item.uagb_featured_image_src?.['list-block']?.[0]})`,
                        }}
                      />
                      <HalfCircle
                        // className="hover:animate-spin"
                        style={{
                          width: '71px',
                          height: '83px',
                          position: 'absolute',
                          top: '-10px',
                          left: '4px',
                        }} />
                    </div> */}
                    <p
                      style={{
                        background: 'linear-gradient(45deg, #ff1e1e, #ffd400)',
                        'WebkitBackgroundClip': 'text',
                      }}
                      className="font-bold text-lg text-center text-transparent leading-5"
                    >
                      {item.title.rendered}
                    </p>
                  </a>
                </Link>
              </div>
            );
          })
        }
      </div>
  )
}

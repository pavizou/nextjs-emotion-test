import Link from 'next/link';
import { css } from '@emotion/react';

const Header = () => {
  return (
    <div
    className="dark:bg-gray-900 bg-red-300"
      css={css`
        padding: 32px;
        font-size: 24px;
        border-radius: 4px;
        &:hover {
          color: white;
        }
      `}
    >
      <div><Link href="/"><a>Nextjs + Emotion test - Home</a></Link></div>
    </div>
  )
};

export default Header;

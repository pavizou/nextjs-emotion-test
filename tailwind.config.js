module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      boxShadow: {
        cards: '#000000a6 -12px 0px 19px 0px, #000000a6 12px 0px 19px 0px',
        'cards-light': '#0000002e -14px 0px 8px 0px',
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            blockquote:{
              marginLeft: '2rem',
            },
            a: {
              color: theme('colors.red.400'),
              '&:hover': {
                color: theme('colors.red.500'),
              },
            },
          },
        },
        dark: {
          css: {
            color: theme('colors.gray.200'),
            a: {
              color: theme('colors.red.400'),
              '&:hover': {
                color: theme('colors.red.500'),
              },
            },
            strong: {
              color: theme('colors.gray.100'),
            },
            blockquote: {
              color: theme('colors.gray.100'),
            },
            h1: {
              color: theme('colors.gray.100'),
            },
            h2: {
              color: theme('colors.gray.100'),
            },
            h3: {
              color: theme('colors.gray.100'),
            },
          },
        },
      }),
    },
  },
  variants: {
    extend: {
      animation: ['hover'],
      typography: ['dark'],
      // padding: ['hover'],
      // width: ['hover'],
      // height: ['hover'],
      margin: ['hover'],
      zIndex: ['hover'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

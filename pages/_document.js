import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="fr" className="dark">
        <Head />
        <body className="dark:bg-[#17141d] dark:text-gray-50 bg-gray-100 text-gray-900 sm:px-6 pt-1">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
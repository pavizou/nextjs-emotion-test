// import App from 'next/app'
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import { css, keyframes } from '@emotion/react';

import 'tailwindcss/tailwind.css'

import Header from 'components/Header';
import Spinner from 'components/Spinner';

const gradient = keyframes`
  0%{background-position:0% 50%}
  50%{background-position:100% 50%}
  100%{background-position:0% 50%}
`;

const spin = keyframes`
  0%{transform: rotate(0deg)}
  100%{transform: rotate(360deg)}
`;

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const handleRouteChange = (url) => {
      setLoading(true)
      console.log(
        `App is changing to ${url} `
      )
    }

    const handleRouteComplete = (url) => {
      setLoading(false);
      console.log(
        `App changed to ${url} `
      )
    }

    router.events.on('routeChangeStart', handleRouteChange)
    router.events.on('routeChangeComplete', handleRouteComplete)

    return () => {
      router.events.off('routeChangeStart', handleRouteChange)
      router.events.off('routeChangeComplete', handleRouteComplete)
    }
  }, [])

  return (
    <>
    {
      loading && (
        <div css={
          css`
            display: flex;
            height: 100%;
            position: fixed;
            top: 0px;
            left: 0;
            right: 0;
            align-items: center;
            justify-content: center;
            font-size: 1rem;
            z-index: 50;
            background: #1F2937;
            font-family: sans-serif;
            color: #F9FAFB;
            opacity: 0.9;
            align-content: center;
            text-align: center;
          `
        }>
          <Spinner css={
            css`
              height: 20px;
              width: 20px;
              margin-right: 12px;
              display: block;
              animation: ${spin} 1.5s linear infinite;
            `
          } /> Chargement...
        </div> 
      )
    }
      <Header />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp

import Head from 'next/head';
import { useRouter } from 'next/router';
import { css } from '@emotion/react'

import { pipe } from 'utils/functions';
import NewsHelpers from 'utils/news';

const N_ARTICLES = 5;

const PostView = ({ news }) => {
  const router = useRouter();
  if (router.isFallback) {
    console.log('Is Fallback');
    return <div>Loading...</div>
  }
  const mt = news.metaTags && news.metaTags.map((tag, i) => {
    return <meta key={i} {...tag} />; // eslint-disable-line
  });
  return (
    <>
      <Head>
        <title>
          Nextjs Emotion test | {news.title}
        </title>
        <link rel="apple-touch-icon-precomposed" href="https://hey-alex.fr/wp-content/uploads/2018/08/mobile-icon-alex.png" />
        {
          news.needsTwitter && <script async src="https://platform.twitter.com/widgets.js" />
        }
        {
          news.needsFacebook && <script async crossOrigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&amp;version=v7.0" />
        }
        {
          news.needsInstagram && <script async src="//www.instagram.com/embed.js" />
        }
        news.jsonLd && (
        <script type="application/ld+json">
          {JSON.stringify(news.jsonLd)}
        </script>
        {
          mt
        }
      </Head>
      <article
        className="mt-12 dark:prose-dark prose xl:prose-2xl mx-auto px-3 md:px-0 lg:w-3/5 lg:max-w-full"
        css={css`
          .fluid-video-wrapper { 
              overflow: hidden; 
              padding-bottom: 56.25%; 
              position: relative; 
              height: 0;
              max-width: 100%;
              margin: auto;

              @media (min-width: 1280px) {
                max-width: 80%;
              }
            }
              
            .fluid-video-wrapper iframe {
              left: 0; 
              top: 0; 
              height: 100%;
              width: 100%;
              position: absolute;
            }
            .twitter-tweet.twitter-tweet-rendered {
              margin: auto;
            }
        `}
      >
        <h1>
          {news.title} 
        </h1>
        <div
          dangerouslySetInnerHTML={{ __html: news.leadParagraph}}
        />
        <div 
          dangerouslySetInnerHTML={{ __html: news.content}}
        />
      </article>
    </>
  );
};

export async function getStaticPaths() {
  const res = await fetch(`https://hey-alex.fr/wp-json/wp/v2/posts?context=view&per_page=${N_ARTICLES}&orderby=date&order=desc`);
  const data = await res.json();
  const paths = data.map(post => ({ params: { slug: post.slug } }));
  // console.log('paths', paths);
  return {
    paths,
    fallback: true,
  };
}


export const getStaticProps = async ({ params }) => {
  const res = await fetch(`https://hey-alex.fr/actu/graphql`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query: `
      query MyQuery {
        posts(where: {name: "${params.slug}"}) {
          nodes {
            id: databaseId
            excerpt(format: RENDERED)
            slug
            title
            seo {
              metaDesc
              metaKeywords
              metaRobotsNofollow
              metaRobotsNoindex
              opengraphAuthor
              opengraphDescription
              opengraphModifiedTime
              opengraphPublishedTime
              opengraphPublisher
              opengraphSiteName
              opengraphTitle
              opengraphType
              opengraphUrl
              title
              opengraphImage {
                mediaItemUrl
                mediaDetails {
                  width
                  height
                }
              }
            }
            author {
              node {
                description
                name
                avatar {
                  url
                }
                locale
                id
              }
            }
            content(format: RENDERED)
            date
            dateGmt
            modified
            modifiedGmt
            link
            featuredImage {
              node {
                caption(format: RENDERED)
                srcSet(size: MEDIUM_LARGE)
                sourceUrl(size: MAIN_FULL)
                mainSlider: sourceUrl(size: MAIN_SLIDER)
                mediaDetails {
                  height
                  width
                }
              }
            }
            categories {
              nodes {
                slug
                name
                id: databaseId
              }
            }
          }
        }
        generalSettings {
          language
          url
        }
      }
      `,
    })
  });
  const response = await res.json();

  return {
    props: {
      news: pipe(
        NewsHelpers.setLeadParagraph,
        NewsHelpers.setMetaTags(response.data.generalSettings),
        NewsHelpers.setJsonLd(response.data.generalSettings),
        NewsHelpers.setMetaTitle,
        NewsHelpers.setArtistSlugs,
        NewsHelpers.decodeFieldHtml('title'),
        // NewsHelpers.setArticleSchema,
        NewsHelpers.setNeedExternalSnippet,
        NewsHelpers.adaptHtml,
        NewsHelpers.decodeFieldHtml('content'),
        // NewsHelpers.addForwardedQueryString(queryString, referer),
      )(response.data.posts.nodes[0]),
    },
  };
};

export default PostView;
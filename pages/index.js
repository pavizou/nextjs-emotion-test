import Head from 'next/head';

import FlexGrid from 'components/FlexGrid';
import { HtmlEntities } from 'utils/string';
const N_ARTICLES = 10;

const decodeTitleHtml = (news) => {
  news.title.rendered = HtmlEntities.decode(news.title.rendered);
  return news;
};

const HomePage = ({ data }) => {
  return (
    <>
      <Head>
        <title>
          Un test de Next.js
        </title>
        <meta name="Description" content="A test with Next.js and a WP blog API" />
      </Head>
      <h2 className="font-sans text-3xl font-bold m-5">
        Les {N_ARTICLES} derniers articles de blog
      </h2>
      <FlexGrid items={data} />
    </>
  );
};

export const getStaticProps = async () => {
  const res = await fetch(`https://hey-alex.fr/wp-json/wp/v2/posts?context=view&per_page=${N_ARTICLES}&orderby=date&order=desc`);
  const data = await res.json();

  return {
    props: {
      data: data.map(decodeTitleHtml)
    },
    revalidate: 300,
  };
};

export default HomePage;